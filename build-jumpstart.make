core = 7.x
api = 2

; Core
projects[drupal][version] = 7.x

; Profiles
projects[jumpstarter][type] = "profile"
projects[jumpstarter][download][type] = "get"
projects[jumpstarter][download][url] = "http://drupalcode.org/sandbox/lewisnyman/2186243.git/snapshot/1b279f5.tar.gz"

; Modules
projects[admin_menu][type] = "module"
projects[admin_menu][subdir] = "contrib"
projects[ctools][type] = "module"
projects[ctools][subdir] = "contrib"
projects[devel][type] = "module"
projects[devel][subdir] = "contrib"
projects[search_krumo][type] = "module"
projects[search_krumo][subdir] = "contrib"
projects[backup_migrate][type] = "module"
projects[backup_migrate][subdir] = "contrib"
projects[entity][type] = "module"
projects[entity][subdir] = "contrib"
projects[entityreference][type] = "module"
projects[entityreference][subdir] = "contrib"
projects[token][type] = "module"
projects[token][subdir] = "contrib"
projects[views][type] = "module"
projects[views][subdir] = "contrib"
projects[module_filter][type] = "module"
projects[module_filter][subdir] = "contrib"
projects[menu_block][type] = "module"
projects[menu_block][subdir] = "contrib"
projects[admin_views][type] = "module"
projects[admin_views][subdir] = "contrib"
projects[views_bulk_operations][type] = "module"
projects[views_bulk_operations][subdir] = "contrib"
projects[google_analytics][type] = "module"
projects[google_analytics][subdir] = "contrib"
projects[pathauto][type] = "module"
projects[pathauto][subdir] = "contrib"
projects[globalredirect][type] = "module"
projects[globalredirect][subdir] = "contrib"
projects[date][type] = "module"
projects[date][subdir] = "contrib"
projects[link][type] = "module"
projects[link][subdir] = "contrib"
projects[ckeditor][type] = module
projects[ckeditor][version] = 1.13
projects[ckeditor][subdir] = contrib

; Libraries.
libraries[ckeditor][download][type] = get
libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor%20for%20Drupal/edit/ckeditor_4.3.2_edit.zip
