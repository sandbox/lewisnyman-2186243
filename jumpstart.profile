<?php

/**
 * @file
 * Implements hook_form_FORM_ID_alter().
 * Allows the profile to alter the site configuration form.
 */

/**
 * Alter the configuration form.
 */
function jumpstart_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name.
  $form['site_information']['site_name']['#default_value'] = 'Jumpstart';
}
